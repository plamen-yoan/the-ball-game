﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackButton : MonoBehaviour
{
    public Canvas canvas;

    public void BackToMenu()
    {
        canvas.enabled = false;
        SceneManager.LoadScene("Menu");
    }
}
