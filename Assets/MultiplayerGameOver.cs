﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Network;

public class MultiplayerGameOver : MonoBehaviour
{
    public Text winnerText;
    LobbyManager manager;

    private void Start()
    {
        this.setText();
    }

    public void BackToLobby()
    {
        SceneManager.LoadScene("Menu");
    }

    private void setText()
    {
        winnerText.text = "WINNER: PLAYER " + JoystickMultiplayerGameOverCondition.winner;
    }
}
