﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour
{
    public void Multiplayer()
    {
        SceneManager.LoadScene("NetworkLobby");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
