﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour {
    public void PlayModeSelectionScene()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        SceneManager.LoadScene("ModeSelection");
    }
}
