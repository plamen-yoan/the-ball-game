﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScorePrinter : MonoBehaviour
{
    private string mode;
    private int currentScoreCount;
    private int bestScoreCount;


    public Text currentScore;
    public Text bestScore;

    void Start()
    {
        currentScoreCount = PlayerPrefs.GetInt("Score");
        mode = PlayerPrefs.GetString("Mode");

        if (mode == "CircleMode")
        {
            bestScoreCount = PlayerPrefs.GetInt("BestCirle", 0);
        }
        else if (mode == "Accelerometer")
        {
            bestScoreCount = PlayerPrefs.GetInt("BestAccelerometer", 0);
        }
        else if (mode == "JoystickMode")
        {
            bestScoreCount = PlayerPrefs.GetInt("BestJoystick", 0);
            print(bestScoreCount);
        }

        if (currentScoreCount > bestScoreCount)
        {
            bestScoreCount = currentScoreCount;
            if (mode == "CircleMode")
            {
                PlayerPrefs.SetInt("BestCirle", bestScoreCount);
                PlayerPrefs.Save();
            }
            else if (mode == "Accelerometer")
            {
                PlayerPrefs.SetInt("BestAccelerometer", bestScoreCount);
                PlayerPrefs.Save();

            }
            else if (mode == "JoystickMode")
            {
                PlayerPrefs.SetInt("BestJoystick", bestScoreCount);
                print("neshto " + bestScoreCount);
                PlayerPrefs.Save();

            }
        }

        currentScore.text = "SCORE: " + currentScoreCount.ToString();
        bestScore.text = "BEST SCORE: " + bestScoreCount.ToString();
    }
}
