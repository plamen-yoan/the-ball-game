﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonMethods : MonoBehaviour
{
    public static string SelectedMode { get; private set; }

    public int modesCount;
    public Button leftArrow;
    public Button rightArrow;

    public Text text;

    public int MenuID { get; private set; }

    private void Start()
    {
        if (SceneManager.GetActiveScene().name != "GameOverScreen")
        {
            this.leftArrow.GetComponent<Button>();
            this.rightArrow.GetComponent<Button>();
            this.leftArrow.interactable = false;

            this.text.GetComponent<Text>();

            this.MenuID = 1;
        }

    }

    private void ChangeMode()
    {
        if (this.MenuID == 1)
        {
            Camera.main.transform.position = new Vector3(-5, 15, 22);
            this.text.text = "Joystick Mode";
        }
        else if (this.MenuID == 2)
        {
            Camera.main.transform.position = new Vector3(30, 15, 22);
            this.text.text = "Clockwise Mode";
        }
        else if (this.MenuID == 3)
        {
            Camera.main.transform.position = new Vector3(65, 22, 23);
            this.text.text = "Gyroscope Mode";
        }
    }

    private void ChangeBall()
    {
        Camera.main.transform.position = new Vector3((MenuID * 5) - 15, Camera.main.transform.position.y, Camera.main.transform.position.z);
    }

    public void RightArrowClick()
    {
        this.MenuID++;
        if (this.MenuID >= modesCount)
        {
            this.MenuID = modesCount;
            this.rightArrow.interactable = false;
            this.leftArrow.interactable = true;
        }
        else
        {
            this.leftArrow.interactable = true;
        }

        if (SceneManager.GetActiveScene().name == "ModeSelection")
        {
            this.ChangeMode();
        }
        else if (SceneManager.GetActiveScene().name == "BallSelection")
        {
            this.ChangeBall();
        }
    }

    public void LeftArrowClick()
    {
        this.MenuID--;
        if (this.MenuID <= 1)
        {
            this.MenuID = 1;
            this.leftArrow.interactable = false;
            this.rightArrow.interactable = true;
        }
        else
        {
            this.rightArrow.interactable = true;
        }

        if (SceneManager.GetActiveScene().name == "ModeSelection")
        {
            this.ChangeMode();
        }
        else if (SceneManager.GetActiveScene().name == "BallSelection")
        {
            this.ChangeBall();
        }
    }

    public void PickMode()
    {
        if (this.MenuID == 1)
        {
            SelectedMode = "JoystickMode";
        }
        else if (this.MenuID == 2)
        {
            SelectedMode = "CircleMode";
        }
        else if (this.MenuID == 3)
        {
            SelectedMode = "Accelerometer";
        }

        SceneManager.LoadScene("BallSelection");
    }

    public void PickBall()
    {
        if (this.MenuID <= 6)
        {
            PlayerPrefs.SetString("BallMaterial", "Ball_C_" + MenuID.ToString());
        }
        else if (this.MenuID == 7)
        {
            PlayerPrefs.SetString("BallMaterial", "Ball_C_19");
        }
        else if (this.MenuID == 8)
        {
            PlayerPrefs.SetString("BallMaterial", "Ball_C_13");
        }
        else if (this.MenuID == 9)
        {
            PlayerPrefs.SetString("BallMaterial", "Ball_C_12");
        }
        else if (this.MenuID == 10)
        {
            PlayerPrefs.SetString("BallMaterial", "Ball_C_22");
        }
        else if (this.MenuID == 11)
        {
            PlayerPrefs.SetString("BallMaterial", "Ball_C_18");
        }
        SceneManager.LoadScene(SelectedMode);
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void PlayAgain()
    {
        SceneManager.LoadScene(SelectedMode);
    }
}