﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuBallSpawnScript : MonoBehaviour {
    private List<GameObject> spawnedBalls;

    public Material[] ballMaterials;
    public GameObject[] ballSpawnPoints;
    public GameObject ballToSpawn;

	private void Start () {
        this.spawnedBalls = new List<GameObject>();
        StartCoroutine(this.SpawnPoint());
        
    }

    private IEnumerator SpawnPoint()
    {
        while (true)
        {
            this.SpawnBall();
            yield return new WaitForSeconds(3);
        }
    }

    private void SpawnBall()
    {
        int index = Random.Range(0, 4);
        GameObject newBall = Instantiate(this.ballToSpawn, this.ballSpawnPoints[index].transform.position, this.ballToSpawn.transform.rotation) as GameObject;
        newBall.GetComponent<Renderer>().material = this.ballMaterials[Random.Range(0, this.ballMaterials.Length)];

        this.spawnedBalls.Add(newBall);
    }
}
