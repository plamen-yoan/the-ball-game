﻿using UnityEngine;

public class AccelPlayerMovement : MonoBehaviour {

    public GameObject colliderCube;
    public Rigidbody rb;
    public Collider col;

    private Vector3 directionStorage;

    private Vector3 tilt = Vector3.zero;
    private float speed = 250f;
    private float circ;
    private Vector3 prevPosition;

    private void Start()
    {
        circ = 2 * Mathf.PI * col.bounds.extents.x;
        prevPosition = transform.position;
        tilt.y = -9.81f;
    }

	void Update ()
    {
        directionStorage = rb.velocity.normalized;

        colliderCube.transform.position = new Vector3 (transform.position.x + directionStorage.x * 0.4f, transform.position.y, transform.position.z + directionStorage.z * 0.4f);

        //transform.Translate(Input.acceleration.x * 0.1f , 0, -Input.acceleration.z * 0.1f);

        tilt.z = Input.acceleration.y;
        tilt.x = Input.acceleration.x;

        rb.velocity = tilt.normalized * speed * Time.deltaTime;
    }

    void LateUpdate()
    {
        Vector3 movement = transform.position - prevPosition;

        movement = new Vector3(movement.z, 0, -movement.x);
        transform.Rotate(movement / circ * 360, Space.World);
        prevPosition = transform.position;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            tilt.y = 0;
        }
    }
}
