﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BallEater : MonoBehaviour
{
    private AudioSource eatSound;

    public static int scoreCount;

    public Text scoreText;

    private void Start()
    {
        this.eatSound = GetComponent<AudioSource>();
        scoreCount = 0;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "EnemyBall")
        {
            Destroy(collision.gameObject);

            this.eatSound.Play();

            scoreCount++;
            this.scoreText.text = "Score: " + scoreCount.ToString();
        }
    }
}