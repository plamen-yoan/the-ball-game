﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AccelBallDuplicateSpawn : MonoBehaviour {

    public Material[] ballMaterials;
    public GameObject[] ballSpawnPoints;
    public GameObject ballToSpawn;
    //public Text scoreText;
    private List<GameObject> spawnedBalls;

    void Start()
    {
        spawnedBalls = new List<GameObject>();
        //scoreText.text = "Score: " + spawnedBalls.Count.ToString();
        StartCoroutine(SpawnPoint());
    }

    private IEnumerator SpawnPoint()
    {
        while (spawnedBalls.Count < 10)
        {
            SpawnBall();
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void SpawnBall()
    {
        int index = Random.Range(0, ballSpawnPoints.Length);
        GameObject newBall = Instantiate(ballToSpawn, ballSpawnPoints[index].transform.position, ballToSpawn.transform.rotation) as GameObject;

        newBall.GetComponent<Renderer>().material = ballMaterials[Random.Range(0, ballMaterials.Length)];
        spawnedBalls.Add(newBall);
        //scoreText.text = "Score: " + spawnedBalls.Count.ToString();
    }
}
