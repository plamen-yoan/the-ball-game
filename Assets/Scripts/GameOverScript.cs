﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    private float delay = 1;
    private float timeToEnterGameOver;
    private bool shouldEnterGameOver = false;

    void Update()
    {
        if (SceneManager.GetActiveScene().name == "CircleMode" && shouldEnterGameOver && Time.time >= timeToEnterGameOver)
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("GameOverScreen");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyBall")
        {
            if (SceneManager.GetActiveScene().name == "JoystickMode")
            {
                PlayerPrefs.SetInt("Score", BallSpawnScript.spawnedBallsCount);
                SceneManager.LoadScene("GameOverScreen");
            }
            else if (SceneManager.GetActiveScene().name == "Accelerometer")
            {
                PlayerPrefs.SetInt("Score", BallEater.scoreCount);
                SceneManager.LoadScene("GameOverScreen");
            }
            else if (SceneManager.GetActiveScene().name == "CircleMode")
            {
                PlayerPrefs.SetInt("Score", scoreCircle.scoreCount);
                scoreCircle.scoreCount = 0;
                timeToEnterGameOver = Time.time + delay;
                shouldEnterGameOver = true;
            }

            PlayerPrefs.SetString("Mode", SceneManager.GetActiveScene().name);
        }
    }
}
