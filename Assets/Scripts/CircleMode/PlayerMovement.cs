﻿using UnityEngine;
using System.Collections;
using CnControls;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody rb;
    private Quaternion rotation;
    private Vector3 radius;
    private float currentRotation;
    private bool hasEntered = false;
    private float xRotation;
    private float zRotation;
    private float yRotation;

    public static Vector3 playerPosition;

    void Start()
    {
        this.rb = GetComponent<Rigidbody>();
        this.radius = new Vector3(2.5f, -1.9f, -4.5f);
        this.currentRotation = 0.0f;
    }

    void Update()
    {
        if (!this.hasEntered)
        {
            this.currentRotation -= CnInputManager.GetAxis("Horizontal") * Time.deltaTime * 100;
            this.rotation.eulerAngles = new Vector3(0, this.currentRotation, 0);
            this.rb.transform.position = this.rotation * this.radius;

            playerPosition = this.transform.position;
            //this.rb.transform.rotation = new Quaternion(xRotation, yRotation, zRotation, this.transform.rotation.w);

        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "EnemyBall" && !this.hasEntered)
        {
            this.hasEntered = true;
        }
    }
}
