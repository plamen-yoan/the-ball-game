﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CircleBallSpawnScript : MonoBehaviour
{
    private List<GameObject> spawnedBalls;
    private float timeToSpawnBall = 0.3f;
    public static int ballSpawned;

    public Material[] materials;
    public GameObject ballSpawnPoint;
    public GameObject ballToSpawn;

    private void Start()
    {
        this.spawnedBalls = new List<GameObject>();
        StartCoroutine(SpawnPoint());
    }

    private void Update()
    {

        for (int i = this.spawnedBalls.Count - 1; i >= 0; i--)
        {
            if (this.spawnedBalls[i].transform.position.y < -10)
            {
                Destroy(this.spawnedBalls[i]);
                this.spawnedBalls.RemoveAt(i);
            }
        }
    }

    private IEnumerator SpawnPoint()
    {
        while (true)
        {
            ballSpawned++;
            this.SpawnBall();
            yield return new WaitForSeconds(this.timeToSpawnBall);
        }
    }

    private void SpawnBall()
    {
            GameObject newBall = Instantiate(this.ballToSpawn, this.ballSpawnPoint.transform.position, this.ballToSpawn.transform.rotation) as GameObject;
            newBall.GetComponent<Renderer>().material = this.materials[Random.Range(0, this.materials.Length)];
            this.spawnedBalls.Add(newBall);

            if (this.timeToSpawnBall >= 0.1)
            {
                this.timeToSpawnBall -= 0.0003f;
            }
        }
}
