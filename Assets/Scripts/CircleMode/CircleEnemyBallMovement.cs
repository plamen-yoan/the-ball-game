﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CircleEnemyBallMovement : MonoBehaviour
{
    private float speed;
    private Vector3 direction;

    public Rigidbody rb;
    public bool isVisible;
    public GameObject playerBall;
    public ParticleSystem particleSystem;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name != "Menu" && SceneManager.GetActiveScene().name != "ModeSelection" && CircleBallSpawnScript.ballSpawned % 7 == 0 && CircleBallSpawnScript.ballSpawned != 0)
        {
            this.direction = new Vector3(PlayerMovement.playerPosition.normalized.x, -1f, PlayerMovement.playerPosition.normalized.z);
        }
        else
        {
            this.direction = new Vector3(Random.Range(-1f, 1f), -1f, Random.Range(-1f, 1f));
        }

        this.isVisible = true;
        this.rb = GetComponent<Rigidbody>();
        this.speed = Random.Range(10f, 14f);

    }

    private void Update()
    {
        if (this != null)
        {
            this.rb.velocity = this.direction * this.speed;
        }

        if (transform.position.y < -10)
        {
            this.rb.velocity = new Vector3(this.rb.velocity.x, this.rb.velocity.y - 4.5f, this.rb.velocity.z);
        }
    }

    void OnBecameInvisible()
    {
        this.isVisible = false;
    }
}