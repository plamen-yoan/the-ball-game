﻿using UnityEngine;
using UnityEngine.UI;

public class scoreCircle : MonoBehaviour
{
    public Text scoreText;

    public static int scoreCount;

    void FixedUpdate()
    {
        scoreCount = (int)Time.timeSinceLevelLoad;
        scoreText.text = "Score: " + scoreCount.ToString();
    }
}
