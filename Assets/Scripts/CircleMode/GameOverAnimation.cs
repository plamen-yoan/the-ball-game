﻿using UnityEngine;
using System.Collections;

public class GameOverAnimation : MonoBehaviour {

    public GameObject BoomParticle;
    public Camera mainCam;

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "EnemyBall")
        {
            BoomParticle.transform.position = transform.position;
            BoomParticle.active = true;
            Time.timeScale = 0.4f;
            mainCam.transform.position = new Vector3(transform.position.x, 5f, transform.position.z);
    
        }
    }
}
