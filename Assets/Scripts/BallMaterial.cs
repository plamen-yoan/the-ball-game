﻿using UnityEngine;

public class BallMaterial : MonoBehaviour {
    private Material newMaterial;

    public GameObject ball;

    void Start () {
        this.newMaterial = Resources.Load(PlayerPrefs.GetString("BallMaterial"), typeof(Material)) as Material;

        this.ball.GetComponent<Renderer>().material = newMaterial;
    }
}
