﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class JoystickMultiplayerGameOverCondition : NetworkBehaviour
{
    public static int winner;

    public void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "EnemyBall")
        {
            if (isServer)
            {
                winner = 2;

                NetworkServer.Shutdown();
                Network.Disconnect();
                NetworkServer.DisconnectAll();
                SceneManager.LoadScene("MultiplayerGameOver");
            }
            else
            {
                winner = 1;

                NetworkServer.Shutdown();
                Network.Disconnect();
                NetworkServer.DisconnectAll();
                SceneManager.LoadScene("MultiplayerGameOver");
            }
        }
    }
}
