﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MPBallEater : MonoBehaviour
{
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "EnemyBall")
        {
            NetworkServer.Destroy(collision.gameObject);
        }
    }
}