﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class MPAccelBallSpawn : NetworkBehaviour {

    //public Material[] ballMaterials;
    public GameObject[] ballSpawnPoints;
    public GameObject ballToSpawn;
    //public Text scoreText;
    private List<GameObject> spawnedBalls;

    public override void OnStartServer()
    {
        spawnedBalls = new List<GameObject>();
        //scoreText.text = "Score: " + spawnedBalls.Count.ToString();
        StartCoroutine(SpawnPoint());
    }

    private IEnumerator SpawnPoint()
    {
        while (true)
        {
            SpawnBall();
            yield return new WaitForSeconds(1.5f);
        }
    }

    private void SpawnBall()
    {
        int index = Random.Range(0, ballSpawnPoints.Length);
        //GameObject newBall = Instantiate(ballToSpawn, ballSpawnPoints[index].transform.position, ballToSpawn.transform.rotation) as GameObject;

        //newBall.GetComponent<Renderer>().material = ballMaterials[Random.Range(0, ballMaterials.Length)];
        //spawnedBalls.Add(newBall);
        //scoreText.text = "Score: " + spawnedBalls.Count.ToString();

        NetworkServer.Spawn(Instantiate(ballToSpawn, ballSpawnPoints[index].transform.position, ballToSpawn.transform.rotation) as GameObject);
    }
}
