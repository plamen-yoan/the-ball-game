﻿using UnityEngine;
using CnControls;

public class AccelPlayerMovementJoystick : MonoBehaviour
{
    private Rigidbody rb;

    public float speedMultiplier;

    void Start()
    {
        this.rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        this.rb.velocity = new Vector3(CnInputManager.GetAxis("Horizontal") * this.speedMultiplier,
                            this.rb.velocity.y, CnInputManager.GetAxis("Vertical") * this.speedMultiplier);

    }
}
