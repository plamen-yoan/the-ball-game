﻿using UnityEngine;

public class MPAccelPlayerMovement : MonoBehaviour {

    public GameObject colliderCube;
    public Rigidbody rb;

    private Vector3 directionStorage;  

	void Update ()
    {
        directionStorage = rb.velocity.normalized;

        colliderCube.transform.position = new Vector3 (transform.position.x + directionStorage.x * 0.4f, transform.position.y, transform.position.z + directionStorage.z * 0.4f);
    }
}
