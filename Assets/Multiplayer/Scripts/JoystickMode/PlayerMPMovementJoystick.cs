﻿using UnityEngine;
using CnControls;

public class PlayerMPMovementJoystick : MonoBehaviour
{
    public static bool hasTakenPowerUp = false;

    private float powerUpExpirationTime;
    private Rigidbody rb;

    public GameObject flameParticles;
    public float speedMultiplier;
    public float powerUpDuration;

    void Start()
    {
        this.rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        this.rb.velocity = new Vector3(CnInputManager.GetAxis("Horizontal") * this.speedMultiplier,
                            this.rb.velocity.y, CnInputManager.GetAxis("Vertical") * this.speedMultiplier);

        if (Time.time >= this.powerUpExpirationTime)
        {
            hasTakenPowerUp = false;
            flameParticles.active = false;
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PowerUp")
        {
            Destroy(collision.gameObject);

            hasTakenPowerUp = true;

            flameParticles.active = true;

            this.powerUpExpirationTime = Time.time + powerUpDuration;
        }
    }
}
