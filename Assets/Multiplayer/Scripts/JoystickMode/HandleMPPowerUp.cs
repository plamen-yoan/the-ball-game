﻿using UnityEngine;

public class HandleMPPowerUp : MonoBehaviour
{
    public float lifeDuration;
    private float timeToDie;

    void Start()
    {
        this.timeToDie = Time.time + lifeDuration;
    }

    void Update()
    {
        this.transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);

        if (Time.time >= this.timeToDie)
        {
            this.transform.position = new Vector3(0, -200, 0);
        }
    }
}
