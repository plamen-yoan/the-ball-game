﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;

public class BallMPSpawnScript : NetworkBehaviour
{
    private List<GameObject> spawnedBalls;

    public static int spawnedBallsCount;

    //public Material[] ballMaterials;
    public GameObject[] ballSpawnPoints;
    public GameObject ballToSpawn;
    //public Text scoreText;

    public override void OnStartServer()
    {
        this.spawnedBalls = new List<GameObject>();
        //this.scoreText.text = "Score: " + this.spawnedBalls.Count.ToString();
        this.StartCoroutine(this.SpawnPoint());
        spawnedBallsCount = 0;
    }

    private IEnumerator SpawnPoint()
    {
        while (true)
        {
            this.SpawnBall();
            yield return new WaitForSeconds(3);
        }
    }

    private void SpawnBall()
    {
        int index = Random.Range(0, 4);
        //GameObject newBall = Instantiate(this.ballToSpawn, this.ballSpawnPoints[index].transform.position, this.ballToSpawn.transform.rotation) as GameObject;

        //newBall.GetComponent<Renderer>().material = this.ballMaterials[Random.Range(0, this.ballMaterials.Length)];
        //this.spawnedBalls.Add(newBall);
        //this.scoreText.text = "Score: " + this.spawnedBalls.Count.ToString();
        //spawnedBallsCount = this.spawnedBalls.Count;
        NetworkServer.Spawn(Instantiate(this.ballToSpawn, this.ballSpawnPoints[index].transform.position, this.ballToSpawn.transform.rotation) as GameObject);
    }
}
