﻿using UnityEngine;

public class EnemyMPBallMovement : MonoBehaviour
{
    private Vector3 oldVelocity;

    private float randomX = 0;
    private float randomZ = 0;
    private float speed;
    private float slowSpeed;

    private bool hasEntered = false;

    public Rigidbody rb;

    public GameObject sparkParticle;

    private void Start()
    {
        this.rb.GetComponent<Rigidbody>();
        this.rb.useGravity = false;
    }

    private void FixedUpdate()
    {
        this.oldVelocity = this.rb.velocity;

        if (!PlayerMovementJoystick.hasTakenPowerUp)
        {
            this.rb.velocity = this.speed * (this.rb.velocity.normalized);
        }
        else
        {
            this.rb.velocity = this.slowSpeed * (this.rb.velocity.normalized);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground" && !this.hasEntered)
        {
            this.hasEntered = true;

            this.speed = Random.Range(5f, 12f);
            this.slowSpeed = this.speed / 2;
            this.randomX = Random.Range(-1f, 1f);
            this.randomZ = Random.Range(-1f, 1f);

            this.rb.velocity = new Vector3(this.randomX, 0, this.randomZ) * this.speed;
        }

        //if (collision.gameObject.tag == "WallXLeft")
        //{
        //    rb.velocity = Vector3.Reflect(-oldVelocity, Vector3.right);
        //}
        //else if (collision.gameObject.tag == "WallXRight")
        //{
        //    rb.velocity = Vector3.Reflect(-oldVelocity, Vector3.left);
        //}

        //if (collision.gameObject.tag == "WallZUp")
        //{
        //    rb.velocity = Vector3.Reflect(-oldVelocity, Vector3.back);
        //}
        //else if (collision.gameObject.tag == "WallZDown")
        //{
        //    rb.velocity = Vector3.Reflect(-oldVelocity, Vector3.forward);
        //}

        if (collision.gameObject.tag == "EnemyBall")
        {
            GameObject particle = Instantiate(this.sparkParticle, this.transform.position, this.transform.rotation) as GameObject;

            Destroy(particle, particle.GetComponent<ParticleSystem>().startLifetime);
        }
    }
}
