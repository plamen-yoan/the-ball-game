﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

public class PowerMPUpSpawn : NetworkBehaviour
{
    public GameObject powerUpToSpawn;
    public GameObject[] spawnPoints;

    private List<GameObject> spawnedPowerUps;

    public override void OnStartServer()
    {

        this.StartCoroutine(SpawnPoint());
    }

    private IEnumerator SpawnPoint()
    {
        while (true)
        {
            this.SpawnPowerUp();
            yield return new WaitForSeconds(20);
        }
    }

    private void SpawnPowerUp()
    {
        NetworkServer.Spawn(Instantiate(powerUpToSpawn, spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position, transform.rotation) as GameObject);
    }
}
