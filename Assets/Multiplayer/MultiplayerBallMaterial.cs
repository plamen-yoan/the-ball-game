﻿using System.Collections;
using UnityEngine;

public class MultiplayerBallMaterial : MonoBehaviour
{
    public GameObject enemyBall;
    public Material[] materials;

    private void Start()
    {
        StartCoroutine(ChangeBall());
    }

    private IEnumerator ChangeBall()
    {
        while (true)
        {
            enemyBall.GetComponent<Renderer>().material = materials[Random.Range(0, materials.Length)];
            yield return new WaitForSeconds(1.4f);
        }
    }
}
